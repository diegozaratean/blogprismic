class PostsController < ApplicationController
  before_action :set_post, only: %i[ edit update destroy ]

  # GET /posts or /posts.json
  def index
    @posts = Post.all
    # api = Prismic.api('https://blogprismicdz.cdn.prismic.io/api/v2')
    # response = api.query(Prismic::Predicates.at("document.type", "page"))
    # documents = response.results
    response = Faraday.get('https://blogprismicdz.cdn.prismic.io/api/v2/documents/search?ref=ZG90TxAAACUAE5Mc')    
    @results = JSON.parse(response.body)['results']
  end

  # GET /posts/1 or /posts/1.json
  def show
    response = Faraday.get('https://blogprismicdz.cdn.prismic.io/api/v2/documents/search?ref=ZG90TxAAACUAE5Mc&q=%5B%5B%3Ad+%3D+at%28document.id%2C+%22' + params[:id] + '%22%29+%5D%5D')    
    @post = JSON.parse(response.body)['results'][0]
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to post_url(@post), notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to post_url(@post), notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.first
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:title, :content)
    end
end
